import asyncio
from bleak import BleakClient, BleakScanner
import PySimpleGUI as sg
import csv
# import matplotlib.pyplot as plt
from datetime import datetime
# import numpy as np
import mqtt
import time

# https://pysimplegui.trinket.io/demo-programs#/demo-programs/all-elements-simple-view

address = None
signal = []
NUM_MEASURES = 12

SEARCHED_DEVICE_NAME = 'PANA-Twin'
handler = None

def change_values_to_str(valores_params):
    if valores_params['Source'] == 0:
        valores_params['Source'] = 'magnetometer'
    elif valores_params['Source'] == 1:
        valores_params['Source'] = 'accelerometer'

    if valores_params['Axis'] == 1:
        valores_params['Axis'] = 'Eje X'
    if valores_params['Axis'] == 2:
        valores_params['Axis'] = 'Eje Y'
    if valores_params['Axis'] == 3:
        valores_params['Axis'] = 'Eje XY'
    if valores_params['Axis'] == 4:
        valores_params['Axis'] = 'Eje Z'
    if valores_params['Axis'] == 7:
        valores_params['Axis'] = 'Vector 3D'

    if valores_params['Report_period'] == 0:
        valores_params['Report_period'] = '1 min'
    if valores_params['Report_period'] == 1:
        valores_params['Report_period'] = '2 min'
    if valores_params['Report_period'] == 3:
        valores_params['Report_period'] = '5 min'
    if valores_params['Report_period'] == 4:
        valores_params['Report_period'] = '10 min'
    if valores_params['Report_period'] == 5:
        valores_params['Report_period'] = '15 min'
    if valores_params['Report_period'] == 6:
        valores_params['Report_period'] = '30 min'
    if valores_params['Report_period'] == 7:
        valores_params['Report_period'] = '60 min'
    return valores_params

def change_values_to_int(valores_params):
    if valores_params['Source'] == 'magnetometer':
        valores_params['Source'] = 0
    elif valores_params['Source'] == 'accelerometer':
        valores_params['Source'] = 1

    if valores_params['Axis'] == 'Eje X':
        valores_params['Axis'] = 1
    if valores_params['Axis'] == 'Eje Y':
        valores_params['Axis'] = 2
    if valores_params['Axis'] == 'Eje XY':
        valores_params['Axis'] = 3
    if valores_params['Axis'] == 'Eje Z':
        valores_params['Axis'] = 4
    if valores_params['Axis'] == 'Vector 3D':
        valores_params['Axis'] = 7

    if valores_params['Report_period'] == '1 min':
        valores_params['Report_period'] = 0
    if valores_params['Report_period'] == '2 min':
        valores_params['Report_period'] = 1
    if valores_params['Report_period'] == '5 min':
        valores_params['Report_period'] = 3
    if valores_params['Report_period'] == '10 min':
        valores_params['Report_period'] = 4
    if valores_params['Report_period'] == '15 min':
        valores_params['Report_period'] = 5
    if valores_params['Report_period'] == '30 min':
        valores_params['Report_period'] = 6
    if valores_params['Report_period'] == '60 min':
        valores_params['Report_period'] = 7
    return valores_params

lista = []
def notification_handler(sender, data):
    """Simple notification handler which prints the data received."""
    datos_enviar = {'datos': int.from_bytes(data, 'big')}
    handler.send_PANA_data(datos_enviar)
    # lista.append(data)
    # print("{0}: {1}".format(sender, data))

async def connect_PANA():
    devices = await BleakScanner.discover()
    print('****************Discovered devices****************')
    for d in devices:
        if d.name == SEARCHED_DEVICE_NAME:
            address = d.address
        print(d)
    client = BleakClient(address)

    print('****************Conectando****************')
    try:
        await client.connect()
        pass
    except Exception as e:
        print(e)
    return client

async def get_values(client):
    valores = []
    chars_interes = [23, 26, 29, 32, 35, 38, 41, 44, 47]
    for char in chars_interes:
        valores.append(int.from_bytes((await client.read_gatt_char(char)), 'big'))
    return valores

async def update_values_PANA(client, valores):
    chars_interes = [23, 26, 29, 32, 35, 38, 41, 44, 47]
    params_names = ['Str_TO', 'Source', 'Thr_Start', 'Thr_Stop', 'Timeout', 'Time_min', 'Buff_size', 'Axis', 'Report_period']
    params_size = [4,1,2,2,4,4,1,1,1]
    for char, param_name, param_size in zip(chars_interes, params_names,params_size):
        value_bytes = valores[param_name].to_bytes(param_size, 'big')
        await client.write_gatt_char(char, value_bytes)
    return client, valores

async def activate_notif_PANA(client):
    char_acc_magneto = 20
    await client.start_notify(char_acc_magneto, notification_handler)
async def deactivate_notif_PANA(client):
    char_acc_magneto = 20
    await client.stop_notify(char_acc_magneto, notification_handler)

if __name__ == '__main__':
    cliente = asyncio.run(connect_PANA())
    valores = asyncio.run(get_values(cliente))
    valores_int = dict(zip(['Str_TO', 'Source', 'Thr_Start', 'Thr_Stop', 'Timeout', 'Time_min', 'Buff_size', 'Axis', 'Report_period'],valores))
    parametros = change_values_to_str(valores_int)

    use_custom_titlebar = True if sg.running_trinket() else False

    def make_window(theme=None):

        NAME_SIZE = 23

        def name(name):
            dots = NAME_SIZE - len(name) - 2
            return sg.Text(name + ' ' + '•' * dots, size=(NAME_SIZE, 1), justification='r', pad=(0, 0),
                           font='Courier 10')

        sg.theme(theme)

        # NOTE that we're using our own LOCAL Menu element
        if use_custom_titlebar:
            Menu = sg.MenubarCustom
        else:
            Menu = sg.Menu

        layout_l = [
            [name('Text'), sg.Text('Text')],
            [sg.Checkbox('Activa envio')],
            [name('Streaming Timeout'), sg.Input(s=5), sg.Text(str(parametros['Str_TO']))],
            [name('Source'), sg.Combo(['magnetometer', 'accelerometer'], default_value=(parametros['Source']), s=(15, 2),
                enable_events=True, readonly=True, k='-SOURCE-')],
            [name('Threshold start'), sg.Input(s=5), sg.Text(str(parametros['Thr_Start']))],
            [name('Threshold stop'), sg.Input(s=5), sg.Text(str(parametros['Thr_Stop']))],
            [name('Timeout'), sg.Input(s=5), sg.Text(str(parametros['Timeout']))],
            [name('Time min'), sg.Input(s=5), sg.Text(str(parametros['Time_min']))],
            [name('Buffer size'), sg.Canvas(background_color=sg.theme_button_color()[1], size=(125, 40))],
            [name('Axis'), sg.Combo(['Eje X', 'Eje Y', 'Eje Z', 'Plano XY', 'Vector 3D'], default_value=parametros['Axis'],
                s=(15, 5), enable_events=True, readonly=True, k='-AXIS-')],
            [name('Report Period'), sg.Combo(['1 min', '2 min', '5 min', '10 min', '15 min', '30 min', '60 min'],
                default_value=parametros['Report_period'], s=(15, 7), enable_events=True, readonly=True, k='-PERIOD-')]]

        layout_r = [
                    [sg.Button('UPDATE VALUES')],
                    [sg.Button('STREAMING')],
                    [sg.Button('Cuenta Coches')]
                    ]

        # Note - LOCAL Menu element is used (see about for how that's defined)
        layout = [
                  [Menu([['File', ['Exit']], ['Edit', ['Edit Me', ]]], k='-CUST MENUBAR-', p=0)],
                  [sg.T('PANA APP - Introduce los valores, ACTUALIZA y cambia al estado deseado', font='_ 14', justification='c',
                        expand_x=True)],
                  [sg.Col(layout_l, p=0), sg.Col(layout_r, p=0)]
                  ]

        window = sg.Window('The PySimpleGUI Element List', layout, finalize=True,
                           right_click_menu=sg.MENU_RIGHT_CLICK_EDITME_VER_EXIT, keep_on_top=True,
                           use_custom_titlebar=use_custom_titlebar)
        return window


    handler = mqtt.SallenTBDevice(host='tb.lab-01.howlab.es', port=1883, name='Maquina_Sallen', access_token='p8xGAhCPXodaLKX6mCVr')
    handler.connect()
    time.sleep(0.5)
    handler.check_files_to_send()
    # handler.send_PANA_data()

    window = make_window()

    while True:
        event, values = window.read()
        # sg.Print(event, values)
        if event == sg.WIN_CLOSED or event == 'Exit':
            break

        if values['-SOURCE-'] != parametros['Source']:
            parametros['Source'] = values['-SOURCE-']
        if values['-AXIS-'] != parametros['Axis']:
            parametros['Axis'] = values['-AXIS-']
        if values['-PERIOD-'] != parametros['Report_period']:
            parametros['Report_period'] = values['-PERIOD-']

        if event == 'UPDATE VALUES':
            # Escribe los valores en las chars
            for num, param in zip([1,2,3,4,5], ['Str_TO', 'Thr_Start', 'Thr_Stop', 'Timeout', 'Time_min']):
                if values[num] != "":
                    parametros[param] = int(values[num])
            valores_updt = change_values_to_int(parametros)
            sg.theme(values['-PERIOD-'])
            window.close()
            asyncio.run(update_values_PANA(cliente,valores_updt))
            valores = asyncio.run(get_values(cliente))
            valores_int = dict(
                zip(['Str_TO', 'Source', 'Thr_Start', 'Thr_Stop', 'Timeout', 'Time_min', 'Buff_size', 'Axis',
                     'Report_period'], valores))
            parametros = change_values_to_str(valores_updt)
            window = make_window()
        elif event == 'STREAMING':
            window.close()
            # todo: Activar las notificaciones de la char del acc, pone un timer para ver cuándo desconectar
            # poner a 1 el CCCD
            asyncio.run(activate_notif_PANA(cliente))
            # tomar referencia del tiempo
            tiempo_cero = datetime.now()
            # empezar while loop del que se sale al superar el tiempo
            time.sleep(30)
            # while((datetime.now()-tiempo_cero).seconds < 30):
            #     pass

            # handler.loopclient.loop_start()
            asyncio.run(deactivate_notif_PANA(cliente))
            sg.theme(values['-PERIOD-'])

            window = make_window()
        elif event == 'Cuenta Coches':
            # todo: desconectar, volver a conectar y activar notificaciones
            sg.theme(values['-PERIOD-'])
            window.close()
            window = make_window()

    window.close()
