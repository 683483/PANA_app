from tb_device_mqtt import TBDeviceMqttClient, TBPublishInfo
import logging
import sys
from concurrent.futures import ThreadPoolExecutor

import time
import os
import glob
import numpy as np
from datetime import datetime


def get_tb_timestamp(timestamp=False):
    if not timestamp or type(timestamp) is not datetime:
        timestamp = datetime.now()

    tb_timestamp = int(timestamp.timestamp() * 1000)
    return tb_timestamp


def get_tb_telemetry_json(values: dict, timestamp=False):
    tb_timestamp = get_tb_timestamp(timestamp)
    telemetry_json = {
        'ts': tb_timestamp,
        'values': values
    }
    return telemetry_json

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

class SallenTBDevice:
    name: str = None
    label: str = None

    dir_name = ''

    __access_token = None
    __provisioning_device_key = None
    __provisioning_secret_key = None

    __client: TBDeviceMqttClient = None

    __registered = False
    __host = 'localhost'
    __port = 1883

    __process_request = None

    __shared_attributes: dict = None
    __client_attributes: dict = None

    nombres_ind_billete = ['media', 'mediana', 'max', 'min', 'std', 'skew', 'curtosis']
    nombres_medidas_CIS = ['dobles1', 'dobles2', 'v_int', 'v_aux']
    nombres_medidas_mot = ['I_trans', 'I_alim', 'n_pulsos_trans', 'n_pulsos_alim', 'T_IR1a1b', 'T_IR1b3a', 'T_IR3a3b',
                           'T_IR1b2']
    nombres_atributos = ['ID_Maquina', 'ID_Bolsa', 'ID_Deposito', 'numero_billetes', 'numero_fajos']

    file_mot_name = ''
    file_CIS_name = ''

    num_fajos = 0
    last_deposit = None
    ##################MOTORES#######################

    SIZE_HEADER_T = 7

    #########INDICADORES DE BILLETE#########
    SIZE_IND_T = 2
    SIZE_IND_I = 16
    SIZE_IND_16b = 14
    size_indicadores_billete_por_medida = [SIZE_IND_I, SIZE_IND_I, SIZE_IND_16b, SIZE_IND_16b, SIZE_IND_T, SIZE_IND_T,
                                           SIZE_IND_T, SIZE_IND_T]
    motores_origins_indicadores = [0]
    for element in size_indicadores_billete_por_medida[:-1]:
        motores_origins_indicadores.append(element + motores_origins_indicadores[-1])

    #########INDICADORES POR FAJO#########
    SIZE_IND_FAJO = 2

    #####################CIS######################
    SIZE_HEADER = 7;
    # SIZE_DATOS_MAQUINA = 1036;
    SIZE_INDICADORES_BILLETE = 116;
    SIZE_INDICADORES_FAJO = 788;

    DOBLES_SAMPLES = 80;
    SIZE_DOBLES_SAMPLES = 4;
    V_SAMPLES = 98;
    SIZE_V_SAMPLES = 4;
    TEMP_SAMPLES = 1;
    SIZE_TEMP_SAMPLES = 4;
    EXTRA_BYTE_BANKNOTE = 0;

    AUDIO_SAMPLES = 10;
    SIZE_AUDIO_SAMPLES = 4;

    SIZE_DATOS_MAQUINA = (2 * DOBLES_SAMPLES * SIZE_DOBLES_SAMPLES \
                          + 2 * V_SAMPLES * SIZE_V_SAMPLES + TEMP_SAMPLES * SIZE_TEMP_SAMPLES + EXTRA_BYTE_BANKNOTE);

    @property
    def is_connected(self):
        return self.__client is not None and self.__client.is_connected()

    def __init__(self, host, port, name, label=None, access_token=None, provision_key=None, provision_secret=None,
                 on_rpc_request=None, client_attributes_keys=[], shared_attributes_keys=[],
                 dir_name='Data_deposit/20_banknotes/'):
        self.__host = host
        self.__port = port

        self.name = name
        self.label = label or name

        self.__access_token = access_token
        self.__provisioning_device_key = provision_key
        self.__provisioning_secret_key = provision_secret

        self.__shared_attributes: dict = {key: None for key in client_attributes_keys}
        self.__client_attributes: dict = {key: None for key in shared_attributes_keys}

        if callable(on_rpc_request):
            self.__process_request = on_rpc_request

        self.dir_name = dir_name

    def check_files_to_send(self):
        envio = False
        # Get list of all files only in the given directory
        list_of_files = filter(os.path.isfile, glob.glob(self.dir_name + '*'))
        # Sort list of files based on last modification time in ascending order
        list_of_files = sorted(list_of_files, key=os.path.getmtime, reverse=True)
        # Iterate over sorted list of files and print file path
        # along with last modification time of file
        for file_path in list_of_files:
            timestamp_str = time.strftime('%m/%d/%Y :: %H:%M:%S', time.gmtime(os.path.getmtime(file_path)))
            print(timestamp_str, ' -->', file_path)
        if self.last_deposit is None or self.last_deposit < int(list_of_files[0][-10:-8]):
            if self.last_deposit is None:
                num_new_files = 1
            else:
                num_new_files = int(list_of_files[0][-10:-8]) - self.last_deposit
            for num in range(num_new_files-1,-1,-1):
                if 'mcb.dat' == list_of_files[num*2][-7:]:
                    self.file_mot_name = list_of_files[num*2]
                    self.file_CIS_name = list_of_files[num*2+1]
                else:
                    self.file_CIS_name = list_of_files[num*2]
                    self.file_mot_name = list_of_files[num*2+1]
                with open(self.file_mot_name, 'rb') as datos_mot:
                    data = datos_mot.read()
                number_banknotes = int.from_bytes(data[:2], signed=False, byteorder='big')
                self.num_fajos = self.num_fajos + 1
                atributos = {'ID_Maquina': self.file_mot_name[-32:-19],
                             'ID_Bolsa': self.file_mot_name[-18:-11],
                             'ID_Deposito': self.file_mot_name[-10:-8],
                             'numero_fajos': self.num_fajos,
                             'numero_billetes': number_banknotes}
                self.update_attributes(atributos)
                self.read_indicadores_motores(data, number_banknotes)
                with open(self.file_CIS_name, 'rb') as datos_CIS:
                    data = datos_CIS.read()
                content = self.read_indicadores_fajo_CIS(data)
                self.update_telemetry(values=content)
            self.last_deposit = int(list_of_files[0][-10:-8])

    def register(self, host=None, port=None, access_token=None, provision_key=None, provision_secret=None):
        if self.__registered:
            return

        logger.info('Registering...')
        if not host:
            host = self.__host
        if not port:
            port = self.__port
        if not provision_key:
            provision_key = self.__provisioning_device_key
        if not provision_secret:
            provision_secret = self.__provisioning_secret_key
        if not access_token:
            access_token = self.__access_token

        credentials = TBDeviceMqttClient.provision(host=host, port=port,
                                                   provision_device_key=provision_key,
                                                   provision_device_secret=provision_secret,
                                                   device_name=self.name, access_token=access_token)
        if credentials == access_token:
            self.__host = host
            self.__port = port
            self.__access_token = access_token
            self.__provisioning_device_key = provision_key
            self.__provisioning_secret_key = provision_secret
            self.__registered = True
            logger.info('Done...')
            return True
        else:
            logger.warning('Error while registering!')
            return False

    def connect(self, host=None, port=None, access_token=None):
        self.disconnect()

        if not host:
            host = self.__host
        if not port:
            port = self.__port
        if not access_token:
            access_token = self.__access_token

        client = TBDeviceMqttClient(host=host, port=port, token=access_token)

        logger.info('Connecting...')
        client.connect()
        if not client.is_connected():
            logger.warning('Error while connecting!')
            return False

        self.__client = client

        client.set_server_side_rpc_request_handler(self.__on_rpc_request)
        client.subscribe_to_all_attributes(self.__on_attributes_updated)
        client.request_attributes(client_keys=list(self.__client_attributes.keys()),
                                  callback=self.__on_attributes_updated)

        logger.info('Done...')

        return self.is_connected

    def disconnect(self):
        if self.is_connected:
            logger.info('Disconnecting...')
            self.__client.set_server_side_rpc_request_handler(None)
            self.__client.disconnect()
            logger.info('Done.')

        self.__client = None

    def __on_rpc_request(self, client, request_id, request_body):

        logger.debug(f'Received RPC {request_body}')

        method = request_body['method']
        params = request_body['params']

        if self.__process_request:
            ex = ThreadPoolExecutor()
            ex.submit(self.__process_request, method, params)

    def __on_attributes_updated(self, client, result, exception):

        if exception is not None:
            logger.warning('Error retrieving attributes', exception)
            return

        if not 'client' in result:
            return

        cs_attributes = result['client']
        if 'total_takings' in cs_attributes:
            self.__total_takings = cs_attributes['total_takings']
        if 'coin_pocket' in cs_attributes:
            self.__coin_pocket = cs_attributes['coin_pocket']
        if 'credit' in cs_attributes:
            self.__credit = cs_attributes['credit']
        if 'balls_out' in cs_attributes:
            self.__balls_out = cs_attributes['balls_out']
        if 'total_balls' in cs_attributes:
            self.__total_balls = cs_attributes['total_balls']
        if 'balls_in' in cs_attributes:
            self.__total_balls = self.__balls_out + cs_attributes['balls_in']

    def read_indicadores_motores(self, data, number_banknotes):

        indicadores_mot_billete = {}
        for measure in self.nombres_medidas_mot:
            indicadores_mot_billete[measure] = {}
            if measure[0] == 'T':
                indicadores_mot_billete[measure]['valor'] = np.empty([number_banknotes])
            else:
                if measure[0] == 'I':
                    indicadores_mot_billete[measure]['eficaz'] = np.empty([number_banknotes])
                for name_indicador in self.nombres_ind_billete:
                    indicadores_mot_billete[measure][name_indicador] = np.empty([number_banknotes])

        # Va pasando por todos los billetes detectados
        for n_banknote in range(0, number_banknotes):
            # Va pasando por cada una de las medidas tomadas
            for name_meas, origin in zip(self.nombres_medidas_mot, self.motores_origins_indicadores):
                # Para tener en cuenta el indicador valor eficaz de las medidas de corriente
                if name_meas[0] == 'I':
                    nombres_indicadores = ['eficaz'] + self.nombres_ind_billete
                    signos = [False, False, False, False, True, False, True, True]
                elif name_meas[0] == 'T':
                    nombres_indicadores = ['valor']
                    signos = [False]
                else:
                    nombres_indicadores = self.nombres_ind_billete
                    signos = [False, False, False, False, False, True, True]
                # Va pasando por los indicadores de cada una de las medidas
                for n_indicador, (name_indicador, signo) in enumerate(zip(nombres_indicadores, signos)):
                    start = self.SIZE_HEADER_T + origin + self.SIZE_IND_T * n_indicador + n_banknote * (
                            self.motores_origins_indicadores[-1] + 2)
                    end = start + self.SIZE_IND_T
                    value1 = int.from_bytes(data[start:end], byteorder='little', signed=signo)
                    indicadores_mot_billete[name_meas][name_indicador][n_banknote] = value1

        indicadores_mot_fajo = {}
        for measure in self.nombres_medidas_mot:
            indicadores_mot_fajo[measure] = {}
            if measure[0] == 'T':
                nombres_indicadores = ['valor']
            elif measure[0] == 'I':
                nombres_indicadores = ['eficaz'] + self.nombres_ind_billete
            else:
                nombres_indicadores = self.nombres_ind_billete
            for indicador_fajo in nombres_indicadores:
                indicadores_mot_fajo[measure][indicador_fajo] = {}
                for name_indicador in self.nombres_ind_billete:
                    indicadores_mot_fajo[measure][indicador_fajo][name_indicador] = 0

        # Va pasando por cada una de las medidas tomadas
        for name_meas_f in self.nombres_medidas_mot:
            if name_meas_f[0] == 'I':
                nombres_indicadores_f = ['eficaz'] + self.nombres_ind_billete
            elif name_meas_f[0] == 'T':
                nombres_indicadores_f = ['valor']
            else:
                nombres_indicadores_f = self.nombres_ind_billete
            for name_indicador_f in nombres_indicadores_f:
                if name_indicador_f == 'skew' or name_indicador_f == 'curtosis':
                    signos_b = [True, True, True, True, True, True, True]
                else:
                    signos_b = [False, False, False, False, False, True, True]
                for name_indicador_b, signo_b in zip(self.nombres_ind_billete, signos_b):
                    start = start + 2
                    end = end + 2
                    value2 = int.from_bytes(data[start:end], byteorder='little', signed=signo_b)
                    indicadores_mot_fajo[name_meas_f][name_indicador_f][name_indicador_b] = value2

        self.update_telemetry(values=indicadores_mot_fajo, trama='motores')

        return indicadores_mot_billete, indicadores_mot_fajo

    def read_indicadores_billete_CIS(self, content, number_banknotes):
        indicadores = {}
        indicadores['dobles1'] = {}
        indicadores['dobles1']['media'] = np.empty([number_banknotes])
        indicadores['dobles1']['mediana'] = np.empty([number_banknotes])
        indicadores['dobles1']['max'] = np.empty([number_banknotes])
        indicadores['dobles1']['min'] = np.empty([number_banknotes])
        indicadores['dobles1']['std'] = np.empty([number_banknotes])
        indicadores['dobles1']['skew'] = np.empty([number_banknotes])
        indicadores['dobles1']['curtosis'] = np.empty([number_banknotes])
        indicadores['dobles2'] = {}
        indicadores['dobles2']['media'] = np.empty([number_banknotes])
        indicadores['dobles2']['mediana'] = np.empty([number_banknotes])
        indicadores['dobles2']['max'] = np.empty([number_banknotes])
        indicadores['dobles2']['min'] = np.empty([number_banknotes])
        indicadores['dobles2']['std'] = np.empty([number_banknotes])
        indicadores['dobles2']['skew'] = np.empty([number_banknotes])
        indicadores['dobles2']['curtosis'] = np.empty([number_banknotes])
        indicadores['v_int'] = {}
        indicadores['v_int']['media'] = np.empty([number_banknotes])
        indicadores['v_int']['mediana'] = np.empty([number_banknotes])
        indicadores['v_int']['max'] = np.empty([number_banknotes])
        indicadores['v_int']['min'] = np.empty([number_banknotes])
        indicadores['v_int']['std'] = np.empty([number_banknotes])
        indicadores['v_int']['skew'] = np.empty([number_banknotes])
        indicadores['v_int']['curtosis'] = np.empty([number_banknotes])
        indicadores['v_aux'] = {}
        indicadores['v_aux']['media'] = np.empty([number_banknotes])
        indicadores['v_aux']['mediana'] = np.empty([number_banknotes])
        indicadores['v_aux']['max'] = np.empty([number_banknotes])
        indicadores['v_aux']['min'] = np.empty([number_banknotes])
        indicadores['v_aux']['std'] = np.empty([number_banknotes])
        indicadores['v_aux']['skew'] = np.empty([number_banknotes])
        indicadores['v_aux']['curtosis'] = np.empty([number_banknotes])
        indicadores['temp'] = {}
        indicadores['temp']['media'] = np.empty([number_banknotes])

        first_byte = self.SIZE_HEADER + self.SIZE_INDICADORES_FAJO

        for n_banknote in range(0, number_banknotes):
            first_byte_banknote = first_byte + n_banknote * self.SIZE_INDICADORES_BILLETE

            indicadores['dobles1']['media'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 0:first_byte_banknote + 4], byteorder='little')
            indicadores['dobles1']['mediana'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 4:first_byte_banknote + 8], byteorder='little')
            indicadores['dobles1']['max'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 8:first_byte_banknote + 12], byteorder='little')
            indicadores['dobles1']['min'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 12:first_byte_banknote + 16], byteorder='little')
            indicadores['dobles1']['std'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 16:first_byte_banknote + 20], byteorder='little')
            indicadores['dobles1']['skew'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 20:first_byte_banknote + 24], byteorder='little', signed=True)
            indicadores['dobles1']['curtosis'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 24:first_byte_banknote + 28], byteorder='little', signed=True)

            indicadores['dobles2']['media'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 28:first_byte_banknote + 32], byteorder='little')
            indicadores['dobles2']['mediana'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 32:first_byte_banknote + 36], byteorder='little')
            indicadores['dobles2']['max'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 36:first_byte_banknote + 40], byteorder='little')
            indicadores['dobles2']['min'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 40:first_byte_banknote + 44], byteorder='little')
            indicadores['dobles2']['std'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 44:first_byte_banknote + 48], byteorder='little')
            indicadores['dobles2']['skew'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 48:first_byte_banknote + 52], byteorder='little', signed=True)
            indicadores['dobles2']['curtosis'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 52:first_byte_banknote + 56], byteorder='little', signed=True)

            indicadores['v_int']['media'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 56:first_byte_banknote + 60], byteorder='little')
            indicadores['v_int']['mediana'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 60:first_byte_banknote + 64], byteorder='little')
            indicadores['v_int']['max'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 64:first_byte_banknote + 68], byteorder='little')
            indicadores['v_int']['min'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 68:first_byte_banknote + 72], byteorder='little')
            indicadores['v_int']['std'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 72:first_byte_banknote + 76], byteorder='little')
            indicadores['v_int']['skew'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 76:first_byte_banknote + 80], byteorder='little', signed=True)
            indicadores['v_int']['curtosis'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 80:first_byte_banknote + 84], byteorder='little', signed=True)

            indicadores['v_aux']['media'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 84:first_byte_banknote + 88], byteorder='little')
            indicadores['v_aux']['mediana'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 88:first_byte_banknote + 92], byteorder='little')
            indicadores['v_aux']['max'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 92:first_byte_banknote + 96], byteorder='little')
            indicadores['v_aux']['min'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 96:first_byte_banknote + 100], byteorder='little')
            indicadores['v_aux']['std'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 100:first_byte_banknote + 104], byteorder='little')
            indicadores['v_aux']['skew'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 104:first_byte_banknote + 108], byteorder='little', signed=True)
            indicadores['v_aux']['curtosis'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 108:first_byte_banknote + 112], byteorder='little', signed=True)

            indicadores['temp']['media'][n_banknote] = int.from_bytes(
                content[first_byte_banknote + 112:first_byte_banknote + 116], byteorder='little')

        return indicadores

    def read_subindicadores_fajo_CIS(self, content, n_variable, n_indicador, signo):
        first_byte = self.SIZE_HEADER + n_variable * 7 * 7 * 4 + n_indicador * 7 * 4

        subindicadores_fajo = {}
        if signo == 1:
            subindicadores_fajo['media'] = int.from_bytes(content[first_byte + 0:first_byte + 4], byteorder='little',
                                                          signed=True)
            subindicadores_fajo['mediana'] = int.from_bytes(content[first_byte + 4:first_byte + 8], byteorder='little',
                                                            signed=True)
            subindicadores_fajo['max'] = int.from_bytes(content[first_byte + 8:first_byte + 12], byteorder='little',
                                                        signed=True)
            subindicadores_fajo['min'] = int.from_bytes(content[first_byte + 12:first_byte + 16], byteorder='little',
                                                        signed=True)
            subindicadores_fajo['std'] = int.from_bytes(content[first_byte + 16:first_byte + 20], byteorder='little',
                                                        signed=True)
            subindicadores_fajo['skew'] = int.from_bytes(content[first_byte + 20:first_byte + 24], byteorder='little',
                                                         signed=True)
            subindicadores_fajo['curtosis'] = int.from_bytes(content[first_byte + 24:first_byte + 28],
                                                             byteorder='little', signed=True)
        else:
            subindicadores_fajo['media'] = int.from_bytes(content[first_byte + 0:first_byte + 4], byteorder='little')
            subindicadores_fajo['mediana'] = int.from_bytes(content[first_byte + 4:first_byte + 8], byteorder='little')
            subindicadores_fajo['max'] = int.from_bytes(content[first_byte + 8:first_byte + 12], byteorder='little')
            subindicadores_fajo['min'] = int.from_bytes(content[first_byte + 12:first_byte + 16], byteorder='little')
            subindicadores_fajo['std'] = int.from_bytes(content[first_byte + 16:first_byte + 20], byteorder='little')
            subindicadores_fajo['skew'] = int.from_bytes(content[first_byte + 20:first_byte + 24], byteorder='little',
                                                         signed=True)
            subindicadores_fajo['curtosis'] = int.from_bytes(content[first_byte + 24:first_byte + 28],
                                                             byteorder='little', signed=True)

        return subindicadores_fajo

    def read_indicadores_fajo_CIS(self, content):
        indicadores_fajo = {}
        indicadores_fajo['dobles1'] = {}
        indicadores_fajo['dobles1']['media'] =      self.read_subindicadores_fajo_CIS(content, 0, 0, 0)
        indicadores_fajo['dobles1']['mediana'] =    self.read_subindicadores_fajo_CIS(content, 0, 1, 0)
        indicadores_fajo['dobles1']['max'] =        self.read_subindicadores_fajo_CIS(content, 0, 2, 0)
        indicadores_fajo['dobles1']['min'] =        self.read_subindicadores_fajo_CIS(content, 0, 3, 0)
        indicadores_fajo['dobles1']['std'] =        self.read_subindicadores_fajo_CIS(content, 0, 4, 0)
        indicadores_fajo['dobles1']['skew'] =       self.read_subindicadores_fajo_CIS(content, 0, 5, 1)
        indicadores_fajo['dobles1']['curtosis'] =   self.read_subindicadores_fajo_CIS(content, 0, 6, 1)

        indicadores_fajo['dobles2'] = {}
        indicadores_fajo['dobles2']['media'] =      self.read_subindicadores_fajo_CIS(content, 1, 0, 0)
        indicadores_fajo['dobles2']['mediana'] =    self.read_subindicadores_fajo_CIS(content, 1, 1, 0)
        indicadores_fajo['dobles2']['max'] =        self.read_subindicadores_fajo_CIS(content, 1, 2, 0)
        indicadores_fajo['dobles2']['min'] =        self.read_subindicadores_fajo_CIS(content, 1, 3, 0)
        indicadores_fajo['dobles2']['std'] =        self.read_subindicadores_fajo_CIS(content, 1, 4, 0)
        indicadores_fajo['dobles2']['skew'] =       self.read_subindicadores_fajo_CIS(content, 1, 5, 1)
        indicadores_fajo['dobles2']['curtosis'] =   self.read_subindicadores_fajo_CIS(content, 1, 6, 1)

        indicadores_fajo['v_int'] = {}
        indicadores_fajo['v_int']['media'] =        self.read_subindicadores_fajo_CIS(content, 2, 0, 0)
        indicadores_fajo['v_int']['mediana'] =      self.read_subindicadores_fajo_CIS(content, 2, 1, 0)
        indicadores_fajo['v_int']['max'] =          self.read_subindicadores_fajo_CIS(content, 2, 2, 0)
        indicadores_fajo['v_int']['min'] =          self.read_subindicadores_fajo_CIS(content, 2, 3, 0)
        indicadores_fajo['v_int']['std'] =          self.read_subindicadores_fajo_CIS(content, 2, 4, 0)
        indicadores_fajo['v_int']['skew'] =         self.read_subindicadores_fajo_CIS(content, 2, 5, 1)
        indicadores_fajo['v_int']['curtosis'] =     self.read_subindicadores_fajo_CIS(content, 2, 6, 1)

        indicadores_fajo['v_aux'] = {}
        indicadores_fajo['v_aux']['media'] =        self.read_subindicadores_fajo_CIS(content, 3, 0, 0)
        indicadores_fajo['v_aux']['mediana'] =      self.read_subindicadores_fajo_CIS(content, 3, 1, 0)
        indicadores_fajo['v_aux']['max'] =          self.read_subindicadores_fajo_CIS(content, 3, 2, 0)
        indicadores_fajo['v_aux']['min'] =          self.read_subindicadores_fajo_CIS(content, 3, 3, 0)
        indicadores_fajo['v_aux']['std'] =          self.read_subindicadores_fajo_CIS(content, 3, 4, 0)
        indicadores_fajo['v_aux']['skew'] =         self.read_subindicadores_fajo_CIS(content, 3, 5, 1)
        indicadores_fajo['v_aux']['curtosis'] =     self.read_subindicadores_fajo_CIS(content, 3, 6, 1)

        first_byte = self.SIZE_HEADER + 4 * 7 * 7 * 4
        indicadores_fajo['temp'] = int.from_bytes(content[first_byte + 0:first_byte + 4], byteorder='little')

        return indicadores_fajo

    def indicadores2telemetria(self, indicadores_fajo, trama):
        '''Hace que los indicadores introducidos como diccionarios de diccionarios estén en el formato deseado para la
        telemetria. Cambia las claves y reordena los valores.'''
        telemetria = {}
        if trama == 'CIS':
            nombres_medidas = self.nombres_medidas_CIS
        else:
            nombres_medidas = self.nombres_medidas_mot
        for nombre_medida in nombres_medidas:
            if nombre_medida[0] == 'I':
                nombres_ind_billete = ['eficaz'] + self.nombres_ind_billete
            elif nombre_medida[0:2] == 'T_':
                nombres_ind_billete = ['valor']
            else:
                nombres_ind_billete = self.nombres_ind_billete
            # Indicadores por billete
            for ind_billete in nombres_ind_billete:
                # Indicadores por Fajo
                for ind_fajo in self.nombres_ind_billete:
                    valor = indicadores_fajo[nombre_medida][ind_billete][ind_fajo]
                    msg = '%s-%s.%s' % (nombre_medida, ind_billete, ind_fajo)  # medidas_fajo[cont_medidas]
                    telemetria[msg] = valor
        return telemetria

    def update_attributes(self, attributes):
        if not self.is_connected:
            return False

        logger.debug(f'Sending attributes update {attributes}...')
        result = self.__client.send_attributes(attributes)
        publish_info = result.get()
        if publish_info == TBPublishInfo.TB_ERR_SUCCESS:
            logger.debug('Done sending attributes')
            return True
        else:
            logger.warning(f'Error while sending attributes [publish_info={publish_info}]')
            return False

    def update_telemetry(self, values=None, timestamp=None, trama='CIS'):
        telemetry = self.indicadores2telemetria(values, trama)
        if not self.is_connected:
            return False


        logger.debug(f'Sending telemetry {telemetry}...')
        result = self.__client.send_telemetry(telemetry=telemetry)
        publish_info = result.get()
        if publish_info == TBPublishInfo.TB_ERR_SUCCESS:
            logger.debug('Done sending telemetry')
            return True
        else:
            logger.warning(f'Error while sending telemetry [publish_info={publish_info}]', )
            return False

    def send_PANA_data(self, values=None):
        # ToDo: tomar los datos que vienen en formato lista, hacerlos diccionario, enviarlos por MQTT

        # logger.debug(f'Sending telemetry {telemetry}...')
        result = self.__client.send_telemetry(telemetry=values)
        publish_info = result.get()
        if publish_info == TBPublishInfo.TB_ERR_SUCCESS:
            logger.debug('Done sending telemetry')
            return True
        else:
            logger.warning(f'Error while sending telemetry [publish_info={publish_info}]', )
            return False